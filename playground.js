const utils = require('./dist');

const { Select, Input } = require('enquirer');

const getUtil = async () => {
  const prompt = new Select({
    name: 'name',
    message: 'Pick a function:',
    choices: Object.keys(utils),
  });

  const name = await prompt.run();

  return utils[name];
};

const getArg = async (i) => {
  const input = new Input({
    message: `Enter argument ${i + 1}`,
  });

  return await input.run();
};

const getUtilArgs = async (fn) => {
  const args = [];

  for (let i = 0; i < fn.length; i++) {
    const arg = await getArg(i);

    args.push(JSON.parse(arg));
  }

  return args;
};

const start = async () => {
  try {
    const fn = await getUtil();
    const args = await getUtilArgs(fn);

    const result = fn(...args);

    console.log(result);

    start();
  } catch (e) {}
};

start();

# Twig utility library

This repo contains a library of utility functions that are used by Twig Education.

## Installation

To install, run the following command:

```
npm i
```

## Build
To build the distribution, run the following command:

```
npm run build
```

### Watch
To monitor and build on changes, run the following command:

```
npm run build:watch
```

## Tests
Tests can be run using:

```
npm test
```

## Playground
There is a playground for testing the utility functions from the command line.  This can be accessed by running:

```
npm start
```

**N.B. you must have built the library before running this**

### Usage
The playground will present a list of all exported utility functions. Use the arrow keys to select a function.

```
? Pick a function: … 
▸ chunkEvery
```

Once selected, the playground will then prompt for each of the arguments of the function. You may enter **parsable json** (each argument is run through `JSON.parse`).

```
✔ Pick a function: · chunkEvery
✔ Enter argument 1 · 3
? Enter argument 2 ‣  [1,2,3,4,5,6,7]
```

The result is then logged to the console using `console.log`.

```
✔ Pick a function: · chunkEvery
✔ Enter argument 1 · 3
✔ Enter argument 2 · [1,2,3,4,5,6,7]
[ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7 ] ]
```

You will then be prompted to select again.  To stop you can press `CTRL+C`.

## Utilities

Current utilities available in this library.

### `chunkEvery`

Split an array into slices of the given size. Throws an error on 0 or negative size.

```js
chunkEvery(3, [1, 2, 3, 4, 5, 6, 7]); //=> [[1, 2, 3], [4, 5, 6], [7]]

chunkEvery(-1, [1, 2, 3, 4, 5, 6, 7]); //=> throw Error
chunkEvery(0, [1, 2, 3, 4, 5, 6, 7]); //=> throw Error
```
module.exports = {
  moduleDirectories: ["node_modules", "src"],
  modulePaths: ["src"],
  preset: "ts-jest",
};

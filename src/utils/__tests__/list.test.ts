import { chunkEvery } from 'utils/list';

const testArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

describe('utils > list', () => {
  describe('chunkEvery(number, Array)', () => {
    it('throws an error if number is not positive', () => {
      expect(() => {
        chunkEvery(-1, testArr);
      }).toThrowError('Nth size must be a positive number');
      expect(() => {
        chunkEvery(0, testArr);
      }).toThrowError('Nth size must be a positive number');
    });

    it('returns the correct chunked arrays', () => {
      const expectedArr = [
        [[1], [2], [3], [4], [5], [6], [7], [8], [9], [10]],
        [
          [1, 2],
          [3, 4],
          [5, 6],
          [7, 8],
          [9, 10],
        ],
        [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]],
        [
          [1, 2, 3, 4],
          [5, 6, 7, 8],
          [9, 10],
        ],
        [
          [1, 2, 3, 4, 5],
          [6, 7, 8, 9, 10],
        ],
        [
          [1, 2, 3, 4, 5, 6],
          [7, 8, 9, 10],
        ],
        [
          [1, 2, 3, 4, 5, 6, 7],
          [8, 9, 10],
        ],
        [
          [1, 2, 3, 4, 5, 6, 7, 8],
          [9, 10],
        ],
        [[1, 2, 3, 4, 5, 6, 7, 8, 9], [10]],
      ];

      expectedArr.forEach((expected, i) => {
        const result = chunkEvery(i + 1, testArr);

        expect(result).toEqual(expected);
      });
    });
  });
});

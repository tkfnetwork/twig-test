/**
 * Array helper function that chunks an array by every
 * nth interval
 *
 * @param {number} size
 * @param {Array<*>} list
 */
export const chunkEvery = <T = any>(size: number, list: Array<T> = []): Array<Array<T>> => {
  if (size <= 0) throw new Error('Nth size must be a positive number');

  const result = [];
  let i = 0;

  while (i < list.length) {
    result.push(list.slice(i, (i += size)));
  }

  return result;
};
